# Stage - Modélisation mathématique et environnement - Tutoriel

**Julien Guillod**, [Sorbonne Université](http://www.sorbonne-universite.fr/),
Licence [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/)

Ce tutoriel est accessible en HTML en cliquant sur le bouton suivant:

[![View](https://img.shields.io/badge/view-html-579aca.svg)](https://guillod.org/mediation/stage-cnrs/)

Il peut être exécuté sur gesis.org en cliquant sur le bouton suivant:

[![Binder](https://notebooks.gesis.org/binder/badge_logo.svg)](https://notebooks.gesis.org/binder/v2/gl/guillod%2Fstage-cnrs/master?urlpath=lab/tree/Tutoriel.ipynb)
